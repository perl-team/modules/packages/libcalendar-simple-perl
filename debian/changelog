libcalendar-simple-perl (2.1.0-1) unstable; urgency=medium

  * Import upstream version 2.1.0.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 May 2024 19:54:08 +0200

libcalendar-simple-perl (2.0.3-1) unstable; urgency=medium

  * Import upstream version 2.0.3.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Update debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sun, 01 Oct 2023 03:09:12 +0200

libcalendar-simple-perl (2.0.1-1) unstable; urgency=medium

  * Import upstream version 2.0.1.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Mon, 18 Jan 2021 21:38:13 +0100

libcalendar-simple-perl (2.0.0-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Import upstream version 2.0.0.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Mon, 22 Jun 2020 19:50:43 +0200

libcalendar-simple-perl (1.23-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Add debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Mar 2018 20:59:28 +0100

libcalendar-simple-perl (1.21-2) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Fabrizio Regalli ]
  * Update to 3.9.2 Standards-Version (no changes).
  * Update d/compat to 8
  * Update debhelper to (>= 8)

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Sat, 06 Jun 2015 23:35:28 +0300

libcalendar-simple-perl (1.21-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Use new 3.0 (quilt) source format
  * Refresh to new DEP5 copyright format
  * Standards-Version 3.8.4 (no changes)
  * Use new short debhelper rules format
  * Install pcal as an example, not in /usr/bin

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * debian/copyright: update years of upstream copyright.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 01 May 2010 18:46:11 -0400

libcalendar-simple-perl (1.20-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: adjust copyright notice.
  * Refresh debian/rules, no functional changes; don't install almost empty
    README anymore.
  * debian/control:
    - add libtest-pod-coverage-perl to Build-Depends-Indep
    - move libmodule-build-perl to Build-Depends
    - add /me to Uploaders

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Apr 2008 20:08:51 +0200

libcalendar-simple-perl (1.19-1) unstable; urgency=low

  [ Roberto C. Sanchez ]
  * New upstream version.
  * debian/watch: User more sophisticated expression.
  * debian/copyright: use dist-based URL.
  * debian/control: Update to Standards-Version 3.7.3 (no changes)
  * debian/rules: Prevent installtion of .packlist.
  * debian/control: Fix typo in description and reformat.

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/watch: use dist-based URL.

 -- Roberto C. Sanchez <roberto@debian.org>  Sat, 15 Mar 2008 21:14:44 -0400

libcalendar-simple-perl (1.17-2) unstable; urgency=low

  * Adopted for Debian Perl Group
  * debian/control: Build-Depends: debhelper increased to (>= 5)
  * debian/compat: updated to 5

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 22 Nov 2006 11:46:28 +0100

libcalendar-simple-perl (1.17-1) unstable; urgency=low

  * New upstream release
  * upstream now ships an example script, called pcal, in /usr/bin.
    On Debian this can be found in the directory
    /usr/share/doc/libcalendar-simple-perl/examples/

 -- Stephen Quinney <sjq@debian.org>  Fri, 27 Oct 2006 20:38:34 +0100

libcalendar-simple-perl (1.14-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Fri, 13 Oct 2006 20:35:41 +0100

libcalendar-simple-perl (1.13-2) unstable; urgency=low

  * debian/control:
      - Moved debhelper to Build-Depends
      - Simplified Build-Depends-Indep
  * Switched to my debian.org email address

 -- Stephen Quinney <sjq@debian.org>  Tue, 20 Jun 2006 20:42:32 +0100

libcalendar-simple-perl (1.13-1) unstable; urgency=low

  * New upstream release - build changes only

 -- Stephen Quinney <sjq@debian.org>  Sat, 11 Jun 2005 11:52:35 +0100

libcalendar-simple-perl (1.12-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Sat, 12 Mar 2005 09:58:12 +0000

libcalendar-simple-perl (1.11-1) unstable; urgency=low

  * New upstream release

 -- Stephen Quinney <sjq@debian.org>  Sun,  5 Dec 2004 16:27:17 +0000

libcalendar-simple-perl (1.10-1) unstable; urgency=low

  * New upstream release
  * Changed from MakeMaker to Module::Build so new Build-Dep on
    libmodule-build-perl and appropriate modifications to debian/rules.

 -- Stephen Quinney <sjq@debian.org>  Mon, 22 Nov 2004 19:31:02 +0000

libcalendar-simple-perl (1.09-1) unstable; urgency=low

  * New upstream release - small code changes and more build tests.

 -- Stephen Quinney <sjq@debian.org>  Tue, 26 Oct 2004 20:26:42 +0100

libcalendar-simple-perl (1.08-1) unstable; urgency=low

  *  Inital release, closes: #226918.

 -- Stephen Quinney <sjq@debian.org>  Fri,  9 Jan 2004 12:14:52 +0000
